
const state = () => ( {
	logEvents: false,
	showWd: false,
} );


const mutations = {
	setShowWd (state, value) {
		state.showWd = value;
	},
	setLogging (state, value) {
		state.logEvents = value;
	},
};


export default {
	namespaced: true,
	state,
	mutations
}
