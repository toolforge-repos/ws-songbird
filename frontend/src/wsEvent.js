
let cfg = {
	showWd: false
};

let eventHandlers = [];

function isIp( user ) {
	return /([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+|[0-9a-fA-F:]+])/.test( user );
}

function editSize( edit ) {
	return edit.length.new - ( edit.length.old || 0 );
}

function init() {

	function handleEvent( event, params ) {
		for ( const handler of eventHandlers ) {
			handler( event, params );
		}
	}


	function handleData( data ) {

		function sendEvent( evtName ) {
			handleEvent( evtName, {
				data: data
			} );
		}

		switch (data.server_name) {
			case 'en.wikisource.org':
			{
				if (data.type === 'edit' || data.type === 'new') {

					if ( isIp( data.user ) ) {
						sendEvent( 'ip_edit' );
						break;
					}

					if ( data.title === 'Template:New texts' && editSize( data ) > 20 ) {
						sendEvent( 'new_text' );
						break;
					}

					if ( data.title.startsWith('Wikisource:Scriptorium') && editSize( data ) > 20 ) {
						sendEvent( 'ws_notice' );
						break;
					}

					if (data.namespace === 104) {
						if (data.comment.startsWith( '/* Proofread */')) {
							sendEvent( 'proofread' );
						} else if (data.comment.startsWith( '/* Validated */')) {
							sendEvent( 'validated' );
						} else {
							sendEvent( 'ws_page' );
						}
					} else if ( [ 0, 114 ].indexOf( data.namespace ) !== -1 && data.type === 'new' ) {
						sendEvent( 'ws_main_new');
					} else {
						sendEvent( 'ws_edit' );
					}
				}
				break;
			}
			case 'www.wikidata.org':
			{
				if ( cfg.showWd ) {

					let key;
					if ( data.comment.startsWith( '/* wbsetlabel' ) ) {
						key = 'edit_label'
					} else if ( data.comment.startsWith( '/* wbcreateclaim' ) ) {
						key = 'create_claim'
					} else if ( data.comment.startsWith( '/* wbsetclaim' ) ) {
						key = 'edit_claim'
					}  else if ( data.comment.startsWith( '/* wbsetsitelink' ) ) {
						key = 'edit_sitelink'
					} else {
						key = 'edit'
					}

					if ( data.bot ) {
						key = 'bot_' + key;
					}

					sendEvent( 'wd_' + key );
				}
				break;
			}
			case 'en.wikipedia.org':
			{
				// sendEvent( 'wp_edit' );
				break;
			}
		}
	}

	var setUpEventSource = function() {

		var eventsource = new EventSource('https://stream.wikimedia.org/v2/stream/recentchange');

		eventsource.onmessage = function(msg) {
			// console.log(msg.data);
			handleData(JSON.parse(msg.data));
		};

		eventsource.onerror = function(event) {
			console.error('--- Encountered error', event);

			eventsource.close();
			setUpEventSource( eventsource );
		};
	}

	setUpEventSource();
}

function addEventHandler( cb ) {
	eventHandlers.push( cb );
}

function setShowWikidata( v ) {
	cfg.showWd = v;
}


module.exports = {
	init,
	addEventHandler,
	setShowWikidata
}
