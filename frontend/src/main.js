import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'

import settings from './settingsStore.js'
import stats from './statsStore.js'

import './assets/css/main.css';

Vue.config.productionTip = false

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    settings,
    stats
  }
} );

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
