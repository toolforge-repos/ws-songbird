

const state = () => ( {
    total: 0,
} );


const mutations = {
    addEvent (state, event) {
    	state.total++;
    },
};

export default {
    namespaced: true,
    state,
    mutations
}
