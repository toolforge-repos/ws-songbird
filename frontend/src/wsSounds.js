const { newInstrument } = require( '@dr.pogodin/soundfont-player' );

let ac = new AudioContext();
let instruments = {};

let lastNode;

function play( name, note ) {

	if ( lastNode ) {
		lastNode.stop();
		lastNode = null;
	}

	instruments[ name ].play( note )
		.then( ( audioNode ) => {
			lastNode = audioNode;
		} );
}

function init( baseUrl ) {
	let instrumentPromises = [];

	function loadInstrument( name, options ) {

		const url = baseUrl + `soundfonts/${name}-mp3.js`;

		instrumentPromises.push(
			newInstrument( ac, url, options )
				.then( instrument => {
					instruments[ name ] = instrument;
				} )
		);
	}

	loadInstrument( 'bird_tweet' );
	loadInstrument( 'violin', { attack: 0.5, decay: 0.5 } );
	loadInstrument( 'trumpet', { attack: 0.5, decay: 0.5 } );
	loadInstrument( 'steel_drums' );

	return Promise.all( instrumentPromises );
}

function soundEvent( event, params ) {
	let map = {
		ip_edit: [ 'steel_drums', 'd2' ],
		new_text: [ 'steel_drums', 'e4' ],
		proofread: [ 'bird_tweet', 'c5' ],
		validated: [ 'bird_tweet', 'c6' ],
		ws_page: [ 'bird_tweet', 'a4' ],
		ws_main_new: [ 'koto', 'a4' ],
		ws_notice: [ 'steel_drums', 'f2' ],
		ws_edit: [ 'koto', 'c4' ],

		wd_edit: [ 'violin', 'c3' ],
		wd_edit_label: [ 'violin', 'd3' ],
		wd_edit_claim: [ 'violin', 'e3' ],
		wd_create_claim: [ 'violin', 'g3' ],
		wd_edit_sitelink: [ 'violin', 'a3' ],

		wd_bot_edit: [ 'violin', 'c4' ],
		wd_bot_edit_label: [ 'violin', 'd4' ],
		wd_bot_edit_claim: [ 'violin', 'e4' ],
		wd_bot_create_claim: [ 'violin', 'g4' ],
		wd_bot_edit_sitelink: [ 'violin', 'a4' ],

		wp_edit: [ 'koto', 'f2' ]
	};

	const sound = map[ event ];

	if ( sound ) {
		play( sound[ 0 ], sound[ 1 ] );
	}
}

module.exports = {
	init,
	soundEvent
};
