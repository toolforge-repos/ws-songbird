module.exports = {
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = "Wikisource Songbird";
        return args;
      })
  },

  lintOnSave: false
}
